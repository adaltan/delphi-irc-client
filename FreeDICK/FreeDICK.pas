unit FreeDICK;

{$DEFINE DEBUG}


{ TODO -oyjh13 -cDEBUG : ASSERTIONs }
{ TODO -oyjh13 -cCTCP : Custom CTCP Handling. }
{ TODO -oyjh13 -c? : 가능한 Delimiter들 상수화. }
{ TODO -oyjh13 -cDEBUG : ban, kick에 버그 }
{ TODO -oyjh13 -cParse : FIX ':'  bugs! }
{ TODO -oyjh13 -cevent : Clear Userlist }


interface

uses
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  IdComponent,
  IdBaseComponent,
  IdGlobal,
  IdTelnet,
  uSplitString
  ;

type
        TIRCMessageParsingMode = ( Default, Custom, Both );
        TSimpleNotifyEvent = procedure of object;
        TNotifyWithMessageEvent = procedure (const Msg : String) of object;
        TMessageDeliveryEvent = procedure (const from_who, to_who, msg : String)
                of object;
        TIRCPRIVMessageEvent = TMessageDeliveryEvent;
        TMessageParseEvent =
                procedure (const prefix, command, params : string) of object;
        TUserlistUserChangeNickEvent =
                procedure (const OldNick, NewNick : String) of object;
        TIRCNotifyEvent = TMessageDeliveryEvent;
        TOutputMessageEvent = TNotifyWithMessageEvent;
        TTopicChangeEvent = TNotifyWithMessageEvent;
        TUnknownIRCMessageEvent = TMessageParseEvent;
        TUserlistUserAddEvent = TNotifyWithMessageEvent;
        TUserlistUserRemoveEvent = TNotifyWithMessageEvent;
        TJoinedToChannelEvent = TNotifyWithMessageEvent;
        TPartedToChannelEvent = TNotifyWithMessageEvent;
        TBannedEvent = TNotifyWithMessageEvent;
        TIRCErrorEvent = TNotifyWithMessageEvent;
        TUserKickedEvent = TNotifyWithMessageEvent;
        TUserPartedEvent = TNotifyWithMessageEvent;
        TUserQuitedEvent = TNotifyWithMessageEvent;
        TUserVanishedEvent = TNotifyWithMessageEvent;
        TInvalidNickTryAnotherEvent = TSimpleNotifyEvent;
  TFreeDICK = class(TComponent)
  private
    { Private declarations }
    FIdTelnet   :       TIdTelnet;
    FHost       :       String;
    FPort       :       Integer;
    FChannel    :       String;
    FTopic      :       String;
    FMyRealname   :       String;
    FMyEmail  :       String;
    FMyUsername :       String;
    FMyNick   :       String;
    FUsersList  :       TStringList;
    FParsingMode        :       TIRCMessageParsingMode;
    FOnConnected        :       TClientEvent;
    FOnDisconnected     :       TClientEvent;
    FOnOutputMessage    :       TOutputMessageEvent;
    FOnOutputDebugMessage       :       TOutputMessageEvent;
    FOnUnknownIRCMessage        :       TUnknownIRCMessageEvent;
    FOnTopicChanged     :       TTopicChangeEvent;
    FOnMessageFromUser  :       TIRCPRIVMessageEvent;
    FOnMessageFromChannel       :       TIRCPRIVMessageEvent;
    FOnUserlistUserAdd  :       TUserlistUserAddEvent;
    FOnUserlistUserRemove       :       TUserlistUserRemoveEvent;
    FOnUserlistUserChangeNick   :       TUserlistUserChangeNickEvent;
    FOnIRCNotify        :       TIRCNotifyEvent;
    FOnJoinedToChannel    :       TJoinedToChannelEvent;
    FOnPartedToChannel  :       TPartedToChannelEvent;
    FOnIRCError :       TIRCErrorEvent;
    FOnUserKicked       :       TUserKickedEvent;
    FOnUserParted       :       TUserPartedEvent;
    FOnUserQuited       :       TUserQuitedEvent;
    FOnBanned   :       TBannedEvent;
    FOnUserVanished     :       TUserVanishedEvent;
    FOnInvalidNickTryAnother    :       TInvalidNickTryAnotherEvent;
    FOnMessageParsing   :       TMessageParseEvent;
    procedure SetUsersList(const NewValue : TStringList);
    procedure ParseMsg(Buffer : String);
    procedure CallbackOnDataAvailable(Buffer : String);
    procedure OutputMessage(const Msg : String);
    procedure OutputDebugMessage(const By, Msg : String);
    procedure DoReaction(const Prefix, Command, Params : String);
    procedure DoCTCP(const who_From, who_To, What : String);
    procedure AddToUserlist(const NewNick : String);
    procedure RemoveFromUserlist(const Nick : String);
    procedure ChangeUserNick(const OldNick, NewNick : String);
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    function IsConnected : boolean;
    procedure Connect;
    procedure Disconnect;
    procedure SendMsg(const Msg : String);
    procedure IRC_Regist;
    procedure IRC_JOIN;
    procedure IRC_PART;
    procedure IRC_MessageToUser(const Nick, Msg : String);
    procedure IRC_Message(const Msg : String);
    procedure IRC_NICK;
    procedure IRC_USER;
    procedure IRC_NOTICE(const WhoFrom, Msg : String);
  published
    { Published declarations }
    property Host : String
        read FHost write FHost
        stored true nodefault;
    property Port : Integer
        read FPort write FPort
        stored true default 21;
    property Channel : String
        read FChannel write FChannel
        stored true nodefault;
    property Topic : String
        read FTopic write FTopic
        stored false nodefault;
    property MyRealname : String
        read FMyRealname write FMyRealname
        stored true nodefault;
    property MyEmail : String
        read FMyEmail write FMyEmail
        stored true nodefault;
    property MyNick : String
        read FMyNick write FMyNick
        stored true nodefault;
    property MyUsername : String
        read FMyUsername write FMyUsername
        stored true nodefault;
    property UserList : TStringList
        read FUsersList write SetUsersList
        stored false nodefault;
    property ParsingMode : TIRCMessageParsingMode
        read FParsingMode write FParsingMode
        stored true default Both;
    property OnConnected : TClientEvent
        read FOnConnected write FOnConnected
        stored true;
    property OnDisconnected : TClientEvent
        read FOnDisconnected write FOnDisconnected
        stored true;
    property OnOutputMessage : TOutputMessageEvent
        read FOnOutputMessage write FOnOutputMessage
        stored true;
    property OnOutputDebugMessage : TOutputMessageEvent
        read FOnOutputDebugMessage write FOnOutputDebugMessage
        stored true;
    property OnUnknownIRCMessage : TUnknownIRCMessageEvent
        read FOnUnknownIRCMessage write FOnUnknownIRCMessage
        stored true;
    property OnTopicChanged : TTopicChangeEvent
        read FOnTopicChanged write FOnTopicChanged
        stored true;
    property OnMessageFromUser : TIRCPRIVMessageEvent
        read FOnMessageFromUser write FOnMessageFromUser
        stored true;
    property OnMessageFromChannel : TIRCPRIVMessageEvent
        read FOnMessageFromChannel write FOnMessageFromChannel
        stored true;
    property OnUserlistUserAdd : TUserlistUserAddEvent
        read FOnUserlistUserAdd write FOnUserlistUserAdd
        stored true;
    property OnUserlistUserRemove : TUserlistUserRemoveEvent
        read FOnUserlistUserRemove write FOnUserlistUserRemove
        stored true;
    property OnUserlistUserChangeNick : TUserlistUserChangeNickEvent
        read FOnUserlistUserChangeNick write FOnUserlistUserChangeNick
        stored true;
    property OnIRCNotify : TIRCNotifyEvent
        read FOnIRCNotify write FOnIRCNotify
        stored true;
    property OnJoinedToChannel : TJoinedToChannelEvent
        read FOnJoinedToChannel write FOnJoinedToChannel
        stored true;
    property OnPartedToChannel : TPartedToChannelEvent
        read FOnPartedToChannel write FOnPartedToChannel
        stored true;
    property OnIRCError : TIRCErrorEvent
        read FOnIRCError write FOnIRCError
        stored true;
    property OnUserKicked : TUserKickedEvent
        read FOnUserKicked write FOnUserKicked
        stored true;
    property OnUserParted : TUserPartedEvent
        read FOnUserParted write FOnUserParted
        stored true;
    property OnUserQuited : TUserQuitedEvent
        read FOnUserQuited write FOnUserQuited
        stored true;
    property OnBanned : TBannedEvent
        read FOnBanned write FOnBanned
        stored true;
    property OnInvalidNickTryAnother : TInvalidNickTryAnotherEvent
        read FOnInvalidNickTryAnother write FOnInvalidNickTryAnother
        stored true;
    property OnMessageParsing : TMessageParseEvent
        read FOnMessageParsing write FOnMessageParsing
        stored true;
  end;

const
        ERR_NOSUCHNICK  =       '401';
        ERR_NOSUCHSERVER        =       '402';
        ERR_NOSUCHCHANNEL       =       '403';
        ERR_CANNOTSENDTOCHAN    =       '404';
        ERR_TOOMANYCHANNELS     =       '405';
        ERR_WASNOSUCHNICK       =       '406';
        ERR_TOOMANYTARGETS      =       '407';
        ERR_NOORIGIN    =       '409';
        ERR_NORECIPIENT =       '411';
        ERR_NOTEXTTOSEND        =       '412';
        ERR_NOTOPLEVEL  =       '413';
        ERR_WILDTOPLEVEL        =       '414';
        ERR_UNKNOWNCOMMAND      =       '421';
        ERR_NOMOTD      =       '422';
        ERR_NOADMININFO =       '423';
        ERR_FILEERROR   =       '424';
        ERR_NONICKNAMEGIVEN     =       '431';
        ERR_ERRONEUSNICKNAME    =       '432';
        ERR_NICKNAMEINUSE       =       '433';
        ERR_NICKCOlLISION       =       '436';
        ERR_USERNOTINCHANNEL    =       '441';
        ERR_NOTTOCHANNEL        =       '442';
        ERR_USERONCHANNEL       =       '443';
        ERR_NOLOGIN     =       '444';
        ERR_SUMMONDISABED       =       '445';
        ERR_USERDISABLED        =       '446';
        ERR_NOTREGISTERED       =       '451';
        ERR_NEEDMOREPARAMS      =       '461';
        ERR_ALREADYREGISTERED   =       '462';
        ERR_NOPERMFORHOST       =       '463';
        ERR_PASSWDMISMATCH      =       '464';
        ERR_YOUREBANNEDCREEP     =       '465';
        ERR_KEYSET      =       '467';
        ERR_CHANNELISFULL       =       '471';
        ERR_UNKNOWNMODE =       '472';
        ERR_INVITEONLYCHAN      =       '473';
        ERR_BANNEDFROMCHAN      =       '474';
        ERR_NOPRIVILEGES        =       '481';
        ERR_CHANOPRIVSNEEDED    =       '482';
        ERR_CANTKILLSERVER      =       '483';
        ERR_NOOPERHOST  =       '491';
        ERR_UMODEUNKNOWNFLAG    =       '501';
        ERR_USERDONTMATCH       =       '502';
        RPL_NONE        =       '300';
        RPL_USERHOST    =       '302';
        RPL_ISON        =       '303';
        RPL_AWAY        =       '301';
        RPL_UNAWAY      =       '305';
        RPL_NOWAWAY     =       '306';
        RPL_WHOISUSER   =       '311';
        RPL_WHOISSERVER =       '312';
        RPL_WHOISOPERATOR       =       '313';
        RPL_WHOISIDLE   =       '317';
        RPL_ENDOFWHOIS  =       '318';
        RPL_WHOISCHANNELS       =       '319';
        RPL_WHOWASUSER  =       '314';
        RPL_ENDOFWHOWAS =       '369';
        RPL_LISTSTART   =       '321';
        RPL_LIST        =       '322';
        RPL_LISTEND     =       '323';
        RPL_CHANNELMODEIS       =       '324';
        RPL_NOTOPIC     =       '331';
        RPL_TOPIC       =       '332';
        RPL_INVITING    =       '341';
        RPL_SUMMONING   =       '342';
        RPL_VERSION     =       '351';
        RPL_WHOREPLY    =       '352';
        RPL_ENDOFWHO    =       '315';
        RPL_NAMEREPLY   =       '353';
        RPL_ENDOFNAMES  =       '366';
        RPL_LINKS       =       '364';
        RPL_ENDOFLINKS  =       '365';
        RPL_BANLIST     =       '367';
        RPL_ENDOFBANLIST        =       '368';
        RPL_INFO        =       '371';
        RPL_ENDOFINFO   =       '374';
        RPL_MOTDSTART   =       '375';
        RPL_MOTD        =       '372';
        RPL_ENDOFMOTD   =       '376';
        RPL_YOUREOPER   =       '381';
        RPL_REHASHING   =       '382';
        RPL_TIME        =       '391';
        RPL_USERSTART   =       '392';
        RPL_USERS       =       '393';
        RPL_ENDOFUSERS  =       '394';
        RPL_NOUSERS     =       '395';
        RPL_TRACELINK   =       '200';
        RPL_TRACECONNECTING     =       '201';
        RPL_TRACEHANDSHAKE      =       '202';
        RPL_TRACEUNKNOWN        =       '203';
        RPL_TRACEOPERATOR       =       '204';
        RPL_TRACEUSER   =       '205';
        RPL_TRACESERVER =       '206';
        RPL_TRACENEWTYPE        =       '208';
        RPL_TRACELOG    =       '261';
        RPL_STATSLINKINFO       =       '211';
        RPL_STATSCOMMANDS       =       '212';
        RPL_STATSCLINE  =       '213';
        RPL_STATSNLINE  =       '214';
        RPL_STATSKLINE  =       '215';
        RPL_STATSYLINE  =       '216';
        RPL_ENDOFSTATS  =       '219';
        RPL_STATSLLINE  =       '241';
        RPL_STATSUPTIME =       '242';
        RPL_STATSOLINE  =       '243';
        RPL_STATSHLINE  =       '244';
        RPL_UMODEIS     =       '221';
        RPL_LUSERCLIENT =       '251';
        RPL_LUSEROP     =       '252';
        RPL_LUSERUNKNOWN        =       '253';
        RPL_LUSERCHANNELS       =       '254';
        RPL_LUSERME     =       '255';
        RPL_ADMINME     =       '256';
        RPL_ADMINLOC1   =       '257';
        RPL_ADMINLOC2   =       '258';
        RPL_ADMINEMAIL  =       '259';
        RPL_TRACECLASS  =       '209';
        RPL_STATSQLINE  =       '217';
        RPL_SERVICEINFO =       '231';
        RPL_ENDOFSERVICES       =       '232';
        RPL_SERVICE     =       '233';
        RPL_SERVLIST    =       '234';
        RPL_SERVLISTEND =       '235';
        RPL_WHOISCHANOP =       '316';
        RPL_KILLDONE    =       '361';
        RPL_CLOSING     =       '362';
        RPL_CLOSEEND    =       '363';
        RPL_INFOSTART   =       '373';
        RPL_MYPORTS     =       '384';
        ERR_YOUWILLBEBANNED     =       '466';
        ERR_BADCHANMASK =       '476';
        ERR_NOSERVICEHOST       =       '492';

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('My Components', [TFreeDick]);
end;

constructor TFreeDICK.Create(AOwner : TComponent);
begin
        inherited Create(AOwner);
        //my code.
        FPort := 6667;
        FIdTelnet := TIdTelnet.Create( Self );
        FUsersList := TStringList.Create;
end;

destructor TFreeDICK.Destroy;
begin
        //my code.
        FUsersList.Free;
        //tradition.
        inherited;
end;

procedure TFreeDICK.SetUsersList(const NewValue : TStringList);
begin
        FUsersList.Assign( NewValue );
end;

procedure TFreeDICK.Connect;
begin
        FIdTelnet.OnDataAvailable := CallbackOnDataAvailable;
        FIdTelnet.Port := FPort;
        FIdTelnet.Host := FHost;
        //>>>BEGIN
        {$IFDEF DEBUG}
        OutputDebugMessage('Connect','Port is '+IntToStr(FIdTelnet.Port));
        OutputDebugMessage('Connect','Host is '+FIdTelnet.Host);
        {$ENDIF}
        //>>>END
        FIdTelnet.Connect;
        IRC_Regist;
        //>>>BEGIN
        {$IFDEF DEBUG}
        OutputDebugMessage('Connect','Connection Established!');
        {$ENDIF}
        //>>>END
        if Assigned( FOnConnected ) then
        begin
                //>>>BEGIN
                {$IFDEF DEBUG}
                OutputDebugMessage('Connect','invoke OnConnected Callback.');
                {$ENDIF}
                //>>>END
                FOnConnected;
        end;
end;

procedure TFreeDICK.IRC_NICK;
begin
        Self.SendMsg('NICK '+Self.FMyNick);
end;

procedure TFreeDICK.IRC_USER;
begin
        Self.SendMsg('USER '+FMyUsername+' * * :'+FMyRealname);
end;

procedure TFreeDICK.IRC_Regist;
begin
        //User Registration.
        Self.IRC_USER;
        Self.IRC_NICK;
end;

procedure TFreeDICK.IRC_JOIN;
begin
        Self.SendMsg('JOIN '+Self.FChannel);
end;

procedure TFreeDICK.IRC_PART;
begin
        Self.SendMsg('PART '+Self.FChannel);
end;

procedure TFreeDICK.IRC_NOTICE(const WhoFrom, Msg : String);
begin
        Self.SendMsg('NOTICE '+WhoFrom+' :'+Msg);
end;

procedure TFreeDICK.IRC_MessageToUser(const Nick, Msg : String);
begin
        Self.SendMsg('PRIVMSG '+Nick+' :'+Msg);
end;

procedure TFreeDICK.IRC_Message(const Msg : String);
begin
        Self.SendMsg('PRIVMSG '+FChannel+' :'+Msg);
end;

procedure TFreeDICK.AddToUserlist(const NewNick : String);
begin
        FUsersList.Add(NewNick);
        if Assigned( FOnUserlistUserAdd ) then
        begin
                FOnUserlistUserAdd(NewNick);
        end;
end;

procedure TFreeDICK.RemoveFromUserlist(const Nick : String);
var
        p : integer;
begin
        p:=FUsersList.IndexOf(Nick);
        if p=-1 then
        begin
                //<<<ASSERT>>>
                exit;
        end;
        FUsersList.Delete(p);
        if Assigned( FOnUserlistUserRemove ) then
        begin
                FOnUserlistUserRemove(Nick);
        end;
end;

procedure TFreeDICK.ChangeUserNick(const OldNick, NewNick : String);
var
        p : integer;
begin
        p:=FUsersList.IndexOf(OldNick);
        if p=-1 then
        begin
                //<<<ASSERT>>>
                exit;
        end;
        FUsersList.Delete(p);
        FUsersList.Add(NewNick);
        if Assigned( FOnUserlistUserChangeNick ) then
        begin
                FOnUserlistUserChangeNick(OldNick,NewNick);
        end;
end;

procedure TFreeDICK.Disconnect;
begin
        if FIdTelnet.Connected then
        begin
                //>>>BEGIN
                {$IFDEF DEBUG}
                OutputDebugMessage('Disconnect','closing connection...');
                {$ENDIF}
                //>>>END
                FIdTelnet.Disconnect;
                if Assigned( FOnDisconnected ) then
                begin
                        //>>>BEGIN
                        {$IFDEF DEBUG}
                        OutputDebugMessage('Disconnect','invoke OnDisconnected Callback.');
                        {$ENDIF}
                        //>>>END
                        FOnDisconnected;
                end;
        end
        //>>>BEGIN
        {$IFDEF DEBUG}
        else
        begin
                OutputDebugMessage('Disconnect','disconnected already.');
        end;
        {$ENDIF}
        //>>>END
end;
function TFreeDICK.IsConnected : boolean;
begin
        result:=FIdTelnet.Connected;
end;

procedure TFreeDICK.SendMsg(const Msg : String);
begin
        //>>>BEGIN
        {$IFDEF DEBUG}
        OutputDebugMessage('SendMsg','Msg: ['+Msg+']');
        {$ENDIF}
        //>>>END
        if FIdTelnet.Connected then
        begin
                FIdTelnet.Write(Msg+CR+LF);
                //>>>BEGIN
                {$IFDEF DEBUG}
                OutputDebugMessage('SendMsg','Msg is Sent!');
                {$ENDIF}
                //>>>END
        end
        //>>>BEGIN
        {$IFDEF DEBUG}
        else
        begin
                OutputDebugMessage('SendMsg','connection isnt established!');
        end;
        {$ENDIF}
        //>>>END
end;

procedure TFreeDICK.OutputMessage(const Msg : String);
begin
        if Assigned(FOnOutputMessage) then FOnOutputMessage(Msg);
end;

procedure TFreeDICK.OutputDebugMessage(const By, Msg : String);
begin
        if Assigned(FOnOutputDebugMessage) then
                FOnOutputDebugMessage(By+'@'+DateToStr(Now)+': '+Msg);
end;

procedure TFreeDICK.CallbackOnDataAvailable(Buffer : String);
var
        list : TStringList;
        i : integer;
begin
        ///>>>BEGIN
        {$IFDEF DEBUG}
        OutputDebugMessage('CallbackOnDataAvailable',
                'Bytes:'+IntToStr(length(Buffer)));
        {$ENDIF}
        ///>>>END
        list:=ExplodeString(buffer,EOL);
        i:=0;
        while (i<list.Count) do
        begin
                ///>>>BEGIN
                {$IFDEF DEBUG}
                OutputDebugMessage('CallbackOnDataAvailable',
                        '('+IntToStr(i)+'):['+list.Strings[i]+']');
                {$ENDIF}
                ///>>>END
                ParseMsg(list.Strings[i]);
                inc(i);
        end;
end;

procedure TFreeDICK.ParseMsg(Buffer : String);
var
        buf1,
        buf2,
        prefix,
        command,
        params : string;
begin
        if buffer='' then
        begin
                //>>>BEGIN
                {$IFDEF DEBUG}
                OutputDebugMessage('ParseMsg','Msg is Null');
                {$ENDIF}
                //>>>END
                exit;
        end;
        prefix:='';
        command:='';
        params:='';
        //>>>BEGIN
        {$IFDEF DEBUG}
        OutputDebugMessage('ParseMsg','Msg is ['+Buffer+']');
        {$ENDIF}
        //>>>END
        if copy(buffer,1,1)=':' then
        begin
                SplitString(Buffer,':',buf1,buf2);
                SplitString(buf2,' ',prefix,buf1);
        end
        else
        begin
                buf1:=buffer;
        end;
        SplitString(buf1,' ',command,params);
        //>>>BEGIN
        {$IFDEF DEBUG}
        OutputDebugMessage('ParseMsg','prefix:['+prefix+']');
        OutputDebugMessage('ParseMsg','command:['+command+']');
        OutputDebugMessage('ParseMsg','params:['+params+']');
        {$ENDIF}
        //>>>END
        if (FParsingMode = default) or (FParsingMode = both) then
        begin
                DoReaction(Prefix,Command,Params);
        end;
        if (FParsingMode = custom) or (FParsingMode = both) then
        begin
                if Assigned( FOnMessageParsing ) then
                begin
                        FOnMessageParsing(prefix,command,params);
                end;
        end;
end;

procedure TFreeDICK.DoReaction(const Prefix, Command, Params : String);
var
        buf1, buf2 : String;
        who_to, who_from, msg, chan : String;
        list : TStringList;
        i : integer;
begin
        if command='PING' then
        begin
                Self.SendMsg('PONG '+params);
        end
        else if command='001' then
        begin
                SplitString(Params,' ',buf1,buf2);
                if buf1 = FMyNick then
                begin
                        Self.IRC_JOIN;
                end
                else
                begin
                //<<<ASSERT>>>
                end
        end
        else if command='JOIN' then
        begin
                SplitString(Prefix,'!',who_from,buf2);
                if who_from = FMyNick then
                begin
                        SplitString(Params,':',buf1,buf2);
                        FChannel := buf2;
                        if assigned(FOnJoinedToChannel) then
                        begin
                                FOnJoinedToChannel(FChannel);
                        end;
                end
                else
                begin
                        Self.AddToUserlist(who_from);
                end
        end
        else if command='PART' then
        begin
                SplitString(prefix,'!',who_from,buf2);
                if who_from=FMyNick then
                begin
                        UserList.Clear;
                        if Assigned( FOnPartedToChannel ) then
                        begin
                                FOnPartedToChannel(FChannel);
                        end;
                end
                else
                begin
                        RemoveFromUserlist(who_from);
                        if Assigned( FOnUserParted ) then
                        begin
                                FOnUserParted(who_from);
                        end;
                end;
        end
        else if command='QUIT' then
        begin
                SplitString(Prefix,'!',who_from,buf2);
                RemoveFromUserlist(who_from);
                if Assigned( FOnUserQuited ) then
                begin
                        FOnUserQuited(who_from);
                end;
        end
        else if command='NOTIFY' then
        begin
                if SplitString(Prefix,'!',buf1,buf2)=0 then
                begin
                        who_from := Prefix;
                end
                else
                begin
                        who_from := buf2;
                end;
                SplitString(Params,':',who_to,msg);
                if Assigned(FOnIRCNotify) then
                begin
                        FOnIRCNotify(who_from,who_to,msg);
                end
        end
        else if (command='TOPIC') or (command=RPL_TOPIC) then
        begin
                SplitString(Params,':',buf1,buf2);
                FTopic := buf2;
                if Assigned(FOnTopicChanged) then
                begin
                        FOnTopicChanged(FTopic);
                end;
        end
        else if command=RPL_NOTOPIC then
        begin
                FTopic:='';
                if Assigned(FOnTopicChanged) then
                begin
                        FOnTopicChanged(FTopic);
                end;
        end
        else if command=ERR_NOSUCHNICK then
        begin
                SplitString(params,' ',who_to,buf2);
                RemoveFromUserlist(who_to);
                if assigned(FOnUserVanished) then
                begin
                        FOnUserVanished(who_to);
                end;
        end
        else if command=ERR_YOUREBANNEDCREEP then
        begin
                SplitString(params,':',buf1,msg);
                if assigned(FOnBanned) then
                begin
                        FOnBanned(msg);
                end;
        end
        else if command='PRIVMSG' then
        begin
                SplitString(prefix,'!',who_from,buf1);
                SplitString(params,':',who_to,msg);
                if SplitString(who_to,' ',buf1,buf2)<>0 then
                begin
                        SplitString(who_to,' ',who_to,buf2);
                end;
                {$IFDEF DEBUG}
                OutputDebugMessage('DoReaction','who_from:['+who_from+']');
                OutputDebugMessage('DoReaction','who_to:['+who_to+']');
                OutputDebugMessage('DoReaction','channel:['+fchannel+']');
                OutputDebugMessage('DoReaction','msg:'+msg);
                {$ENDIF}
                if AnsiPos('',msg)<>0 then
                begin
                        SplitString(msg,'',buf1,msg);
                        SplitString(msg,'',msg,buf2);
                        {$IFDEF DEBUG}
                        OutputDebugMessage('DoReaction','msg:['+msg+']');
                        OutputDebugMessage('DoReaction','from:['+who_from+']');
                        OutputDebugMessage('DoReaction','to:['+who_to+']');
                        {$ENDIF}
                        DoCTCP(who_from,who_to,msg);
                end
                else if who_to = FChannel then
                begin
                        if Assigned(FOnMessageFromChannel) then
                        begin
                                FOnMessageFromChannel(who_from,who_to,msg);
                        end;
                end
                else
                begin
                        if Assigned(FOnMessageFromUser) then
                        begin
                                FOnMessageFromUser(who_from,who_to,msg);
                        end;
                end
        end
        else if command='KICK' then
        begin
                SplitString(prefix,'!',who_from,buf2);
                SplitString(params,' ',chan,who_to);
                SplitString(who_to,' ',who_to,buf2);
                if assigned(FOnUserKicked) then
                begin
                        FOnUserKicked(who_to);
                end;
                Self.RemoveFromUserlist(who_to);
        end
        else if command='NICK' then
        begin
                SplitString(prefix,'!',who_from,buf2);
                who_to:=params;
                ChangeUserNick(who_from,who_to);
        end
        else if command='ERROR' then
        begin
                if Assigned( FOnIRCError ) then
                begin
                        FOnIRCError(Params);
                end
        end
        else if (command=ERR_ERRONEUSNICKNAME) or (command=ERR_NICKNAMEINUSE) then
        begin
                if Assigned( FOnInvalidNickTryAnother ) then
                begin
                        FOnInvalidNickTryAnother;
                end;
        end
        else if command=RPL_NAMEREPLY then
        begin
                OutputDebugMessage('DoReaction','NAMEREPLY['+params+']');
                SplitString(params,':',buf1,buf2);
                //list:=TStringList.Create;
                list:=ExplodeString(buf2,' ');
                i:=0;
                while i<list.Count do
                begin
                        who_to:=list.Strings[i];
                        if splitstring(who_to,'@',buf1,buf2)<>0 then
                        begin
                                who_to:=buf2;
                        end;
                        AddToUserlist(who_to);
                        inc(i);
                end;
        end
        else if command=RPL_ENDOFNAMES then
        begin
                //Self.OutputMessage(Params);
        end
        else
        begin
                if Assigned(FOnUnknownIRCMessage) then
                begin
                        FOnUnknownIRCMessage(prefix,command,params);
                end;
        end;
end;

procedure TFreeDICK.DoCTCP(const who_From, who_To, What : String);
var
        buf1, buf2 : String;
begin
        if What='VERSION' then
        begin
                buf1:=''+who_to+' '+'FreeDICK IRC Component by yjh13@orgio.net'+'';
                Self.IRC_NOTICE(who_from,buf1);
        end;
end;

end.
 