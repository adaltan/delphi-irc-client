unit uMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Menus, ActnList, FreeDICK;

type
  TformMain = class(TForm)
    pageOutput: TPageControl;
    sheetOutput: TTabSheet;
    sheetDebug: TTabSheet;
    memoOutput: TMemo;
    memoDebug: TMemo;
    freedick: TFreeDICK;
    actionlistMain: TActionList;
    menuMain: TMainMenu;
    actionQuit: TAction;
    actionConnect: TAction;
    actionDisconnect: TAction;
    Fi1: TMenuItem;
    Quit1: TMenuItem;
    Connection1: TMenuItem;
    Connect1: TMenuItem;
    actionDisconnect1: TMenuItem;
    sheetUsers: TTabSheet;
    listUsers: TListBox;
    editInput: TEdit;
    procedure actionDisconnectExecute(Sender: TObject);
    procedure actionQuitExecute(Sender: TObject);
    procedure actionConnectExecute(Sender: TObject);
    procedure freedickOutputDebugMessage(const Msg: String);
    procedure freedickMessageFromChannel(const from_who, to_who,
      msg: String);
    procedure freedickMessageFromUser(const from_who, to_who, msg: String);
    procedure freedickJoinedToChannel(const Msg: String);
    procedure freedickOutputMessage(const Msg: String);
    procedure freedickIRCError(const Msg: String);
    procedure freedickIRCNotify(const from_who, to_who, msg: String);
    procedure freedickTopicChanged(const Msg: String);
    procedure freedickBanned(const Msg: String);
    procedure freedickUserlistUserAdd(const Msg: String);
    procedure freedickUserlistUserChangeNick(const OldNick,
      NewNick: String);
    procedure freedickUserlistUserRemove(const Msg: String);
    procedure freedickUserKicked(const Msg: String);
    procedure freedickPartedToChannel(const Msg: String);
    procedure freedickUserParted(const Msg: String);
    procedure freedickUserQuited(const Msg: String);
    procedure editInputKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  formMain: TformMain;
  item_point : TPoint;

implementation

{$R *.DFM}

procedure TformMain.actionDisconnectExecute(Sender: TObject);
begin
        if freedick.IsConnected then
        begin
                freedick.Disconnect;
        end
end;

procedure TformMain.actionQuitExecute(Sender: TObject);
begin
        actionDisconnectExecute(Sender);
        Close;
end;

procedure TformMain.actionConnectExecute(Sender: TObject);
var
        buf1, buf2 : String;
begin
        if freedick.IsConnected then exit;
        buf1:=InputBox('?','Host','irc.hanirc.org');
        freedick.Host := buf1;
        buf1:=InputBox('?','Channel','#testme');
        freedick.Channel := buf1;
        buf1:=InputBox('?','Nick','yjh13');
        freedick.MyNick := buf1;
        freedick.Connect;
end;

procedure TformMain.freedickOutputDebugMessage(const Msg: String);
begin
        memoDebug.Lines.Add(msg);
end;

procedure TformMain.freedickMessageFromChannel(const from_who, to_who,
  msg: String);
begin
        memoOutput.Lines.add('<'+from_who+'>: '+msg);
end;

procedure TformMain.freedickMessageFromUser(const from_who, to_who,
  msg: String);
begin
        memoOutput.Lines.add('*'+from_who+'*: '+msg);
end;

procedure TformMain.freedickJoinedToChannel(const Msg: String);
begin
        memooutput.lines.add('*** Joined: '+Msg);
end;

procedure TformMain.freedickOutputMessage(const Msg: String);
begin
        memooutput.lines.add(msg);
end;

procedure TformMain.freedickIRCError(const Msg: String);
begin
        memoOutput.Lines.add(':::ERROR:::['+Msg+']');
end;

procedure TformMain.freedickIRCNotify(const from_who, to_who, msg: String);
begin
        memooutput.lines.add('*** '+from_who+': ['+msg+']');
end;

procedure TformMain.freedickTopicChanged(const Msg: String);
begin
        freedick.Topic := msg;
        memooutput.lines.add('Topic is:['+msg+']');
end;

procedure TformMain.freedickBanned(const Msg: String);
begin
        memoOutput.lines.add('I"m banned Creep!!!');
end;

procedure TformMain.freedickUserlistUserAdd(const Msg: String);
begin
        listUsers.Items.Add(Msg);
end;

procedure TformMain.freedickUserlistUserChangeNick(const OldNick,
  NewNick: String);
begin
        listUsers.Items.Delete( listusers.items.IndexOf(oldnick) );
        listusers.items.add(newnick);
end;

procedure TformMain.freedickUserlistUserRemove(const Msg: String);
begin
        listUsers.Items.Delete( listusers.items.IndexOf(msg) );
end;

procedure TformMain.freedickUserKicked(const Msg: String);
begin
        memoOutput.Lines.add('KICKED: ['+Msg+']');
end;

procedure TformMain.freedickPartedToChannel(const Msg: String);
begin
        memoOutput.lines.add('PARTED: ['+Msg+']');
end;

procedure TformMain.freedickUserParted(const Msg: String);
begin
        memoOutput.Lines.Add('PARTED: ['+MSG+']');
end;

procedure TformMain.freedickUserQuited(const Msg: String);
begin
        memoOutput.Lines.Add('QUITED: ['+MSG+']');
end;

procedure TformMain.editInputKeyPress(Sender: TObject; var Key: Char);
var
        buf : String;
        who_to : String;
        i : integer;
begin
        if (key=#13) and (freedick.IsConnected) then
        begin
                buf := editInput.Text;
                editInput.Text := '';
                if ansipos('/',buf)=1 then
                begin
                        freedick.sendmsg(Copy(buf,2,length(buf)-1));
                end
                else
                begin
                        if listUsers.selcount<>0 then
                        begin
                                i:=0;
                                while i<listusers.items.count do
                                begin
                                        if listUsers.Selected[i] then
                                        begin
                                                who_to:=listusers.items.strings[i];
                                        end;
                                        inc(i);
                                end;
                                freedick.IRC_MessageToUser(who_to,buf);
                        end
                        else freedick.IRC_Message(buf);
                end
        end;
end;

end.
