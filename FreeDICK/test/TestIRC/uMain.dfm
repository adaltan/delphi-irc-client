object formMain: TformMain
  Left = 404
  Top = 311
  Width = 589
  Height = 668
  Caption = 'TestIRC'
  Color = clBtnFace
  Constraints.MinHeight = 200
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = menuMain
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pageOutput: TPageControl
    Left = 8
    Top = 8
    Width = 563
    Height = 576
    ActivePage = sheetOutput
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    object sheetOutput: TTabSheet
      Caption = 'Chat'
      object memoOutput: TMemo
        Left = 8
        Top = 8
        Width = 537
        Height = 533
        TabStop = False
        Anchors = [akLeft, akTop, akRight, akBottom]
        ImeName = '한국어(한글) (MS-IME98)'
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
    object sheetDebug: TTabSheet
      Caption = 'Debug'
      ImageIndex = 1
      object memoDebug: TMemo
        Left = 8
        Top = 8
        Width = 537
        Height = 531
        Anchors = [akLeft, akTop, akRight, akBottom]
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ImeName = '한국어(한글) (MS-IME98)'
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
    object sheetUsers: TTabSheet
      Caption = 'Users'
      ImageIndex = 2
      object listUsers: TListBox
        Left = 8
        Top = 8
        Width = 537
        Height = 529
        Anchors = [akLeft, akTop, akRight, akBottom]
        ImeName = '한국어(한글) (MS-IME98)'
        ItemHeight = 13
        Sorted = True
        TabOrder = 0
      end
    end
  end
  object editInput: TEdit
    Left = 8
    Top = 592
    Width = 561
    Height = 21
    ImeName = '한국어(한글) (MS-IME98)'
    TabOrder = 0
    OnKeyPress = editInputKeyPress
  end
  object freedick: TFreeDICK
    Port = 6667
    MyRealname = 'yun, jonghyouk'
    MyEmail = 'yjh13@orgio.net'
    MyUsername = 'yjh13'
    ParsingMode = Default
    OnOutputMessage = freedickOutputMessage
    OnOutputDebugMessage = freedickOutputDebugMessage
    OnTopicChanged = freedickTopicChanged
    OnMessageFromUser = freedickMessageFromUser
    OnMessageFromChannel = freedickMessageFromChannel
    OnUserlistUserAdd = freedickUserlistUserAdd
    OnUserlistUserRemove = freedickUserlistUserRemove
    OnUserlistUserChangeNick = freedickUserlistUserChangeNick
    OnIRCNotify = freedickIRCNotify
    OnJoinedToChannel = freedickJoinedToChannel
    OnPartedToChannel = freedickPartedToChannel
    OnIRCError = freedickIRCError
    OnUserKicked = freedickUserKicked
    OnUserParted = freedickUserParted
    OnUserQuited = freedickUserQuited
    OnBanned = freedickBanned
    Left = 552
    Top = 592
  end
  object actionlistMain: TActionList
    Left = 520
    Top = 592
    object actionQuit: TAction
      Category = 'File'
      Caption = 'Quit'
      ShortCut = 16465
      OnExecute = actionQuitExecute
    end
    object actionConnect: TAction
      Category = 'Connection'
      Caption = 'Connect'
      OnExecute = actionConnectExecute
    end
    object actionDisconnect: TAction
      Category = 'Connection'
      Caption = 'Disconnect'
      ShortCut = 16452
      OnExecute = actionDisconnectExecute
    end
  end
  object menuMain: TMainMenu
    Left = 488
    Top = 592
    object Fi1: TMenuItem
      Caption = 'File'
      object Quit1: TMenuItem
        Action = actionQuit
      end
    end
    object Connection1: TMenuItem
      Caption = 'Connection'
      object Connect1: TMenuItem
        Action = actionConnect
        ShortCut = 16451
      end
      object actionDisconnect1: TMenuItem
        Action = actionDisconnect
      end
    end
  end
end
