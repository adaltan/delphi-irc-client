program SplitStringTest;
{$APPTYPE CONSOLE}
uses
  SysUtils,
  classes,
  uSplitString;

var
        buf : string;
        str1 : string;
        str2 : string;
        list : TStringList;
        i : Integer;
begin
  //Insert user code here
  writeln(SplitString('foo'+#10+'bar','oo',str1,str2));
  writeln('pre:'+str1);
  writeln('suff:'+str2);
  list:=ExplodeString('1|2|3|dddd','|');
  i:=0;
  while (i<list.Count) do
  begin
        writeln('['+list.Strings[i]+']');
        inc(i);
  end;
  buf:=JoinStrings(list,',');
  writeln(buf);
  write('[Press...]'); ReadLn;
end.