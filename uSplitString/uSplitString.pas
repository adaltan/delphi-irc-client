unit uSplitString;

interface

uses
        classes,
        sysutils
        ;

function SplitString(
        Src : AnsiString;
        Delimiter : AnsiString;
        var Result_Pre : AnsiString;
        var Result_Suff : AnsiString
) : Integer;
function ExplodeString(Src : AnsiString; Delimiter : AnsiString) : TStringList;
function JoinStrings(Src : TStrings; Delimiter : AnsiString) : AnsiString;

implementation

function SplitString(
        Src : AnsiString;
        Delimiter : AnsiString;
        var Result_Pre : AnsiString;
        var Result_Suff : AnsiString
) : Integer;
var
        len_pre : integer;
        len_suff : integer;
        len_src : integer;
        len_del : integer;
        matched_pos : integer;
begin
        len_pre := 0;
        len_src := length(Src);
        len_del := length(Delimiter);
        matched_pos := AnsiPos( Delimiter, Src );
        //not found.
        if matched_pos = 0 then
        begin
                result:=0;
                Exit;
        end;
        //get Result_Pre.
        if matched_pos = 1 then
        begin
                Result_Pre := '';
        end
        else
        begin
                len_pre := matched_pos - 1;
                Result_Pre := Copy( Src, 1, len_pre );
        end;
        //get Result_Suff.
        len_suff := len_src - ( len_pre + len_del );
        if len_suff = 0 then
        begin
                Result_Suff := '';
        end
        else
        begin
                Result_Suff := Copy( Src, matched_pos+len_del, len_suff );
        end;
        //end.
        result:=matched_pos;
end;

function ExplodeString(Src : AnsiString; Delimiter : AnsiString) : TStringList;
var
        list : TStringList;
        buffer : AnsiString;
        buf1, buf2 : AnsiString;
        ret : Integer;
begin
        list:=TStringList.Create;
        buffer:=src;
        while true do
        begin
                ret:=SplitString(buffer,Delimiter,buf1,buf2);
                if ret=0 then
                begin
                        list.Add(buffer);
                        break;
                end
                else
                begin
                        list.add(buf1);
                        buffer:=buf2;
                end;
        end;
        result:=list;
end;

function JoinStrings(Src : TStrings; Delimiter : AnsiString) : AnsiString;
var
        i : Integer;
        buf : AnsiString;
begin
        buf:='';
        i:=0;
        while (i<Src.Count) do
        begin
                if i<>0 then buf:=buf+Delimiter;
                buf:=buf+Src.Strings[i];
                inc(i);
        end;
        Result:=buf;
end;

end.
